﻿namespace CPU_Cache_Replacment___LRU
{
    partial class LRU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRndClear = new System.Windows.Forms.Button();
            this.btnSet = new System.Windows.Forms.Button();
            this.txtL3 = new System.Windows.Forms.TextBox();
            this.txtL2 = new System.Windows.Forms.TextBox();
            this.txtL1 = new System.Windows.Forms.TextBox();
            this.btnRandom = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtlog = new System.Windows.Forms.TextBox();
            this.lbllog = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.lstL1 = new System.Windows.Forms.ListBox();
            this.lstL2 = new System.Windows.Forms.ListBox();
            this.lstL3 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCacheClear = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblRes3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRes2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblRes = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRndClear);
            this.groupBox2.Controls.Add(this.btnSet);
            this.groupBox2.Controls.Add(this.txtL3);
            this.groupBox2.Controls.Add(this.txtL2);
            this.groupBox2.Controls.Add(this.txtL1);
            this.groupBox2.Controls.Add(this.btnRandom);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(613, 261);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Values";
            // 
            // btnRndClear
            // 
            this.btnRndClear.ForeColor = System.Drawing.Color.Red;
            this.btnRndClear.Location = new System.Drawing.Point(549, 227);
            this.btnRndClear.Name = "btnRndClear";
            this.btnRndClear.Size = new System.Drawing.Size(42, 23);
            this.btnRndClear.TabIndex = 17;
            this.btnRndClear.Text = "X";
            this.btnRndClear.UseVisualStyleBackColor = true;
            this.btnRndClear.Click += new System.EventHandler(this.btnRndClear_Click);
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(321, 227);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(75, 23);
            this.btnSet.TabIndex = 10;
            this.btnSet.Text = "Set";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // txtL3
            // 
            this.txtL3.Location = new System.Drawing.Point(416, 19);
            this.txtL3.Multiline = true;
            this.txtL3.Name = "txtL3";
            this.txtL3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtL3.Size = new System.Drawing.Size(175, 202);
            this.txtL3.TabIndex = 9;
            this.txtL3.TextChanged += new System.EventHandler(this.txtL3_TextChanged);
            // 
            // txtL2
            // 
            this.txtL2.Location = new System.Drawing.Point(221, 19);
            this.txtL2.Multiline = true;
            this.txtL2.Name = "txtL2";
            this.txtL2.Size = new System.Drawing.Size(175, 202);
            this.txtL2.TabIndex = 8;
            this.txtL2.TextChanged += new System.EventHandler(this.txtL2_TextChanged);
            // 
            // txtL1
            // 
            this.txtL1.Location = new System.Drawing.Point(26, 19);
            this.txtL1.Multiline = true;
            this.txtL1.Name = "txtL1";
            this.txtL1.Size = new System.Drawing.Size(175, 202);
            this.txtL1.TabIndex = 7;
            this.txtL1.TextChanged += new System.EventHandler(this.txtL1_TextChanged);
            // 
            // btnRandom
            // 
            this.btnRandom.Location = new System.Drawing.Point(221, 227);
            this.btnRandom.Name = "btnRandom";
            this.btnRandom.Size = new System.Drawing.Size(75, 23);
            this.btnRandom.TabIndex = 6;
            this.btnRandom.Text = "Random";
            this.btnRandom.UseVisualStyleBackColor = true;
            this.btnRandom.Click += new System.EventHandler(this.btnRandom_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(684, 31);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(151, 20);
            this.txtSearch.TabIndex = 11;
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(631, 34);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(47, 13);
            this.lblSearch.TabIndex = 12;
            this.lblSearch.Text = "Search :";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(684, 57);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 13;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtlog
            // 
            this.txtlog.Location = new System.Drawing.Point(634, 110);
            this.txtlog.Multiline = true;
            this.txtlog.Name = "txtlog";
            this.txtlog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtlog.Size = new System.Drawing.Size(201, 445);
            this.txtlog.TabIndex = 14;
            // 
            // lbllog
            // 
            this.lbllog.AutoSize = true;
            this.lbllog.Location = new System.Drawing.Point(631, 94);
            this.lbllog.Name = "lbllog";
            this.lbllog.Size = new System.Drawing.Size(28, 13);
            this.lbllog.TabIndex = 15;
            this.lbllog.Text = "Log:";
            // 
            // btnClear
            // 
            this.btnClear.ForeColor = System.Drawing.Color.Red;
            this.btnClear.Location = new System.Drawing.Point(793, 561);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(42, 23);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "X";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lstL1
            // 
            this.lstL1.FormattingEnabled = true;
            this.lstL1.Location = new System.Drawing.Point(26, 51);
            this.lstL1.Name = "lstL1";
            this.lstL1.Size = new System.Drawing.Size(175, 225);
            this.lstL1.TabIndex = 0;
            // 
            // lstL2
            // 
            this.lstL2.FormattingEnabled = true;
            this.lstL2.Location = new System.Drawing.Point(221, 51);
            this.lstL2.Name = "lstL2";
            this.lstL2.Size = new System.Drawing.Size(175, 225);
            this.lstL2.TabIndex = 1;
            // 
            // lstL3
            // 
            this.lstL3.FormattingEnabled = true;
            this.lstL3.Location = new System.Drawing.Point(416, 51);
            this.lstL3.Name = "lstL3";
            this.lstL3.Size = new System.Drawing.Size(175, 225);
            this.lstL3.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "L1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(218, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "L2:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(442, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "L3:";
            // 
            // btnCacheClear
            // 
            this.btnCacheClear.ForeColor = System.Drawing.Color.Red;
            this.btnCacheClear.Location = new System.Drawing.Point(549, 282);
            this.btnCacheClear.Name = "btnCacheClear";
            this.btnCacheClear.Size = new System.Drawing.Size(42, 23);
            this.btnCacheClear.TabIndex = 18;
            this.btnCacheClear.Text = "X";
            this.btnCacheClear.UseVisualStyleBackColor = true;
            this.btnCacheClear.Click += new System.EventHandler(this.btnCacheClear_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCacheClear);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lstL3);
            this.groupBox1.Controls.Add(this.lstL2);
            this.groupBox1.Controls.Add(this.lstL1);
            this.groupBox1.Location = new System.Drawing.Point(12, 279);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(613, 315);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Caches";
            // 
            // lblRes3
            // 
            this.lblRes3.AutoSize = true;
            this.lblRes3.Location = new System.Drawing.Point(263, 607);
            this.lblRes3.Name = "lblRes3";
            this.lblRes3.Size = new System.Drawing.Size(13, 13);
            this.lblRes3.TabIndex = 22;
            this.lblRes3.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(223, 607);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Count:";
            // 
            // lblRes2
            // 
            this.lblRes2.AutoSize = true;
            this.lblRes2.Location = new System.Drawing.Point(169, 607);
            this.lblRes2.Name = "lblRes2";
            this.lblRes2.Size = new System.Drawing.Size(21, 13);
            this.lblRes2.TabIndex = 20;
            this.lblRes2.Text = "0%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(113, 607);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Not Found:";
            // 
            // lblRes
            // 
            this.lblRes.AutoSize = true;
            this.lblRes.Location = new System.Drawing.Point(75, 607);
            this.lblRes.Name = "lblRes";
            this.lblRes.Size = new System.Drawing.Size(21, 13);
            this.lblRes.TabIndex = 18;
            this.lblRes.Text = "0%";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 607);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Found:";
            // 
            // LRU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 629);
            this.Controls.Add(this.lblRes3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblRes2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblRes);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lbllog);
            this.Controls.Add(this.txtlog);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "LRU";
            this.Text = "LRU";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.TextBox txtL3;
        private System.Windows.Forms.TextBox txtL2;
        private System.Windows.Forms.TextBox txtL1;
        private System.Windows.Forms.Button btnRandom;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtlog;
        private System.Windows.Forms.Label lbllog;
        private System.Windows.Forms.Button btnRndClear;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ListBox lstL1;
        private System.Windows.Forms.ListBox lstL2;
        private System.Windows.Forms.ListBox lstL3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCacheClear;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblRes3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRes2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblRes;
        private System.Windows.Forms.Label label6;
    }
}

