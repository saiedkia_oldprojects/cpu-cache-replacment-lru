﻿namespace CPU_Cache_Replacment___LRU
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLRU = new System.Windows.Forms.Button();
            this.btnRR = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLRU
            // 
            this.btnLRU.Location = new System.Drawing.Point(39, 55);
            this.btnLRU.Name = "btnLRU";
            this.btnLRU.Size = new System.Drawing.Size(75, 23);
            this.btnLRU.TabIndex = 0;
            this.btnLRU.Text = "LRU";
            this.btnLRU.UseVisualStyleBackColor = true;
            this.btnLRU.Click += new System.EventHandler(this.btnLRU_Click);
            // 
            // btnRR
            // 
            this.btnRR.Location = new System.Drawing.Point(180, 55);
            this.btnRR.Name = "btnRR";
            this.btnRR.Size = new System.Drawing.Size(75, 23);
            this.btnRR.TabIndex = 1;
            this.btnRR.Text = "Random";
            this.btnRR.UseVisualStyleBackColor = true;
            this.btnRR.Click += new System.EventHandler(this.btnRR_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 153);
            this.Controls.Add(this.btnRR);
            this.Controls.Add(this.btnLRU);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "Main";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLRU;
        private System.Windows.Forms.Button btnRR;
    }
}