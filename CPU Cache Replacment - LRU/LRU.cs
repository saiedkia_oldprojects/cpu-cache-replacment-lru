﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace CPU_Cache_Replacment___LRU
{
    public partial class LRU : Form
    {
        int[] l1 = new int[10];
        int[] l2 = new int[10];
        int[] l3 = new int[20];
        int[] cache = new int[40];
        int[] found = new int[3];
        // --------------------------------------
        int resFound;
        int resNotFound;
        public LRU()
        {
            InitializeComponent();
        }

        private void btnRandom_Click(object sender, EventArgs e)
        {
            txtL1.Text = "";
            txtL2.Text = "";
            txtL3.Text = "";
            txtlog.Text += "Randomaly...!" + System.Environment.NewLine;
            Random rnd = new Random();
            int[] val = new int[40];

            for (int i = 0; i <= 39; i++)
            {
                val[i] = rnd.Next(1, 1000);
                if (i <= 9)
                {
                    txtL1.Text += val[i].ToString() + System.Environment.NewLine;
                }
                if (i >= 10 && i <= 19)
                {
                    txtL2.Text += val[i].ToString() + System.Environment.NewLine;
                }
                if (i >= 20 && i <= 39)
                {
                    txtL3.Text += val[i].ToString() + System.Environment.NewLine;
                }


            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            lstL1.Items.Clear();
            lstL2.Items.Clear();
            lstL3.Items.Clear();
            txtlog.Text += "Data moved to chaches!" + System.Environment.NewLine;
            /////////////////////////////////////
            int i = 0;
            int j = 0;
            for (i = 0; i <= txtL1.Lines.Count() - 2; i++)
            {
                if (txtL1.Lines.GetValue(i).ToString() != string.Empty)
                {
                    lstL1.Items.Add(txtL1.Lines.GetValue(i));
                    cache[j] = int.Parse(txtL1.Lines.GetValue(j).ToString());
                }
                j++;
            }
            for (i = 0; i <= txtL2.Lines.Count() - 2; i++)
            {
                if (txtL2.Lines.GetValue(i).ToString() != "")
                {
                    lstL2.Items.Add(txtL2.Lines.GetValue(i));
                    cache[j] = int.Parse(txtL2.Lines.GetValue(i).ToString());
                }
                j++;
            }
            for (i = 0; i <= txtL3.Lines.Count() - 2; i++)
            {
                if (txtL3.Lines.GetValue(i).ToString() != "")
                {
                    lstL3.Items.Add(txtL3.Lines.GetValue(i));
                    cache[j] = int.Parse(txtL3.Lines.GetValue(i).ToString());
                }
                j++;
            }

        }

        private void btnRndClear_Click(object sender, EventArgs e)
        {
            txtL1.Text = "";
            txtL2.Text = "";
            txtL3.Text = "";
            txtlog.Text += "Randomaly data cleared" + System.Environment.NewLine;
        }

        private void btnCacheClear_Click(object sender, EventArgs e)
        {
            lstL1.Items.Clear();
            lstL2.Items.Clear();
            lstL3.Items.Clear();
            txtlog.Text += "Chache Cleared!" + System.Environment.NewLine;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            found [0]=0;
            found [1]=0;
            found [2]=0;
            string log = "";
            lstL1.SelectedIndex = -1;
            lstL2.SelectedIndex = -1;
            lstL3.SelectedIndex = -1;
            if (lstL1.Items.Count != 0)
            {
                int i = 0;
                for (i = 0; i <= 3; i++)
                {
                    for (i = 0; i <= lstL1.Items.Count - 1; i++)// L1
                    {
                        l1[i] = int.Parse(txtL1.Lines.GetValue(i).ToString());
                        lstL1.SelectedIndex = i;
                        if (lstL1 .SelectedItem.ToString () == txtSearch.Text)
                        {
                            log += "L1 : Find!" + System.Environment.NewLine;
                            found[0] = int.Parse(txtL1.Lines.GetValue(i).ToString());
                            lstL1.SelectedIndex = i;
                            resFound += 1;
                            break;
                        }
                        else
                        {
                            if (i == lstL1.Items.Count - 1)
                            {
                                log += "L1 : Not Found!" + System.Environment.NewLine;
                                found[0] = -1;
                                lstL1.SelectedIndex = -1;
                            }
                        }
                    }
                    for (i = 0; i <= lstL2.Items.Count - 1; i++)//L2
                    {
                        l2[i] = int.Parse(txtL2.Lines.GetValue(i).ToString());
                        lstL2.SelectedIndex = i;
                        if (lstL2.SelectedItem.ToString() == txtSearch.Text)
                        {
                            log += "L2 : Find!" + System.Environment.NewLine;
                            found[1] = int.Parse(txtL2.Lines.GetValue(i).ToString());
                            lstL2.SelectedIndex = i;
                            resFound += 1;
                            break;
                        }
                        else
                        {
                            if (i == lstL2.Items.Count - 1)
                            {
                                log += "L2 : Not Found!" + System.Environment.NewLine;
                                found[1] = -1;
                                lstL2.SelectedIndex = -1;
                            }
                        }
                    }
                    for (i = 0; i <= lstL3.Items.Count - 1; i++)//L3
                    {
                        l3[i] = int.Parse(txtL3.Lines.GetValue(i).ToString());
                        lstL3.SelectedIndex = i;
                        if (lstL3.SelectedItem.ToString() == txtSearch.Text)
                        {
                            log += "L3 : Find!" + System.Environment.NewLine;
                            found[2] = int.Parse(txtL3.Lines.GetValue(i).ToString());
                            lstL3.SelectedIndex = i;
                            resFound += 1;
                            break;
                        }
                        else
                        {
                            if (i == lstL3.Items.Count - 1)
                            {
                                log += "L3 : Not Found!" + System.Environment.NewLine;
                                found[2] = -1;
                                lstL3.SelectedIndex = -1;
                            }
                        }
                    }
                }//end for
            }//End if
            txtlog.Text += log;
            if (found[0] == -1 && found[1] == -1 && found[2] == -1)
            {
                Replace();
            }
            lblRes.Text  = resFound.ToString () + "%";
            lblRes2.Text = resNotFound.ToString () + "%";
            lblRes3.Text = (resNotFound + resFound).ToString();

        }
        private void Replace()
        {
            int i = 39;
            for (i = 39;i >=1; i--)
            {
                cache[i] = cache[i - 1];
            }
            cache[0] =int.Parse (txtSearch .Text );

            lstL1.Items.Clear();
            lstL2.Items.Clear();
            lstL3.Items.Clear();

            for (i = 0; i <= 39; i++)
            {
                if (i<=9)
                {
                    lstL1.Items.Add(cache[i]);
                }
                if (i>=10 && i<=19)
                {
                    lstL2.Items.Add(cache[i]);
                }
                if (i>=20)
                {
                    lstL3.Items.Add(cache [i]);
                }
            }
            resNotFound += 1;
            txtlog.Text += "New data loaded!" + System.Environment.NewLine;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtlog.Text = "";
        }
        #region txtChange Clear Cahche
        private void txtL1_TextChanged(object sender, EventArgs e)
        {
            lstL1.Items.Clear();

        }

        private void txtL2_TextChanged(object sender, EventArgs e)
        {
            lstL2.Items.Clear();
        }

        private void txtL3_TextChanged(object sender, EventArgs e)
        {
            lstL3.Items.Clear();
        }
        #endregion
    }
}
