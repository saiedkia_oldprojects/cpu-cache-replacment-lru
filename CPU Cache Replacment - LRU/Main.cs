﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CPU_Cache_Replacment___LRU
{
    // Saied Salarkia
    // Islamic Azad Univercity Shahre Rey Branch
    // may.2011 - Ordibehesht.1390
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void btnLRU_Click(object sender, EventArgs e)
        {
            LRU lru = new LRU();
            lru.ShowDialog();
        }

        private void btnRR_Click(object sender, EventArgs e)
        {
            RandomData rr = new RandomData();
            rr.ShowDialog();
        }

    }
}
