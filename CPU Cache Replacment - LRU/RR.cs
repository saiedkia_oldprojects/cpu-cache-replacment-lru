﻿using System;
using System.Windows.Forms;

namespace CPU_Cache_Replacment___LRU
{
    public partial class RandomData : Form
    {
        const int SIZE = 20;
        string[] cache = new string[SIZE];
        Random rnd = new Random();
        //int avilable;// # of find
        //int notAvailable;// # of not find
        int count;
        int found = 0;
        int notFound = 0;

        public RandomData()
        {
            InitializeComponent();
        }


        //Random Button
        private void button2_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < SIZE; i++)
            {
                cache[i] = rnd.Next(1, 3000).ToString();
                lstdata.Items.Add(cache[i]);
            }

            txtOutput.Text += "Random data gen..!" + Environment.NewLine;
        }
        
        //Fetch - Search
        private void button1_Click(object sender, EventArgs e)
        {
            if (lstdata.Items.Count == 0) return;

            int Found = 1;
            count++;
            for (int i = 0; i < SIZE; i++)
            {
                if (cache[i] == txtSearch.Text)
                {
                    txtOutput.Text += txtSearch.Text + " - Found" + Environment.NewLine;
                    Found = 1;
                    break;
                }
                else
                {
                    Found = 0;
                }
            }

            if (Found == 0)
            {
                notFound += 1;
                int j = rnd.Next(0, SIZE - 1);
                string LastValue = "";
                lstdata.SelectedIndex = j;
                LastValue = lstdata.SelectedItem.ToString();
                cache[j] = txtSearch.Text;
                lstdata.Items.Clear();
                ///////////////////
                for (int i = 0; i < SIZE; i++)
                {
                    lstdata.Items.Add(cache[i]);
                }
                ///////////////////

                txtOutput.Text += LastValue + " - Changed to " + txtSearch.Text + Environment.NewLine;
                lblRes2.Text = notFound.ToString(); //((count * notFound) / 100) + "%";
            }
            else
            {
                found += 1;

                lblRes.Text = found.ToString();// ((count * found) / 100) + "%";
            }
            lblRes3.Text = count.ToString();
        }


        //Clear
        private void button3_Click(object sender, EventArgs e)
        {
            found = 0;
            notFound = 0;
            count = 0;
            lstdata.Items.Clear();
            lblRes3.Text ="0";
            lblRes2.Text = "0%";
            lblRes .Text ="0%";
            txtSearch.Text = "";
            txtOutput.Text = "";
        }
    }
}
